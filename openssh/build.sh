#!/usr/bin/env bash
set -x
Dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
cd "$Dirname/src" && docker build -t king011/openssh-server:ubuntu-20.04 .
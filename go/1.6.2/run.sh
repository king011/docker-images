#!/usr/bin/env bash

docker run \
    --net=host \
    -v /media/king/程式/docker/golang/home:/home/king \
    -v /media/king/程式/docker/golang/project:/home/king/project \
    -v /media/king/程式/docker/golang/lib:/home/king/lib \
    --rm --name go1.6.2 -d king011/go:1.6.2 \
    /usr/sbin/sshd  -D -p 10022
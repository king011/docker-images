#!/bin/bash
set -e

cd /home/dev/
exec gosu dev "$@"
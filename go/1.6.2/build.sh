#!/usr/bin/env bash
set -x
Dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
cd "$Dirname/src" && docker build -t king011/go:1.6.2 .